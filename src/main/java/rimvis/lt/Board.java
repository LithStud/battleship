package rimvis.lt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class Board {
    // Setting debugger for board is left for Player class
    private Debugger debugger = new Debugger(); // just some default debugger (default: OFF)

    private final int cols = 10;
    private final int rows = 10;
    private Cell[][] board;

    Board() {
        board = new Cell[rows][cols];
        System.out.println("Filling board with empty cells");

        fillWithEmptyCells();
    }

    private void insertShip(int size) {
        // if failed to place ship recursively call itself again
        if (!placeShipRandomly(size)) insertShip(size);
    }

    void placeShips() {
        System.out.println("Adding ships...");
        insertShip(5);
        insertShip(4);
        insertShip(3);
        insertShip(3);
        insertShip(2);
    }

    void fillWithEmptyCells() {
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                board[i][j] = new Cell(j, i, CellState.EMPTY);
    }

    void resetBoard() {
        board = new Cell[rows][cols];
        fillWithEmptyCells();
        //placeShips();
    }

    private void markShipArea(int col, int row) {
        markNeighbours(col, row, CellState.SHIP_AREA);
    }

    private void markNeighbours(int col, int row, CellState state) {
        // cycle around target cell
        for (int targetRow = row - 1; targetRow <= row + 1; targetRow++)
            for (int targetCol = col - 1; targetCol <= col + 1; targetCol++) {
                Cell cell = getCell(targetCol, targetRow);
                // null if out of bounds, mark only EMPTY cells
                if (cell != null && (cell.getState() == CellState.EMPTY || cell.getState() == CellState.SHIP_AREA)) {
                    if (debugger.DEBUG) {
                        System.out.print("Changin " + cell.getCol() + " " + cell.getRow() + " " + cell.getState().name() +
                                " to " + state.name());
                    }
                    // Mark cell
                    cell.setState(state);

                    if (debugger.DEBUG)
                        System.out.println(" -> RESULT: " + getCell(targetCol, targetRow).getState().name());
                }
            }
    }

    private Cell getCell(int col, int row) {
        return col >= 0 && col < cols && row >= 0 && row < rows ? board[row][col] : null;
    }

    /**
     * adds ship to board according to battleship rules
     *
     * @param size Ship size
     * @return returns true if ship is placed onto board, false otherwise
     */
    private boolean placeShipRandomly(int size) {
        Random random = new Random();
        int col = random.nextInt(cols);
        int row = random.nextInt(rows);
        boolean vertical = random.nextBoolean();

        return placeShip(row, col, size, vertical);
    }

    private boolean placeShip(int row, int col, int size, boolean vertical) {
        if (canShipFit(row, col, size, vertical)) {
            for (int i = 0; i < size; i++) {
                if (vertical) {
                    addCellToBoard(col, row + i, CellState.SHIP);
                    markShipArea(col, row + i);
                } else { // horizontal
                    addCellToBoard(col + i, row, CellState.SHIP);
                    markShipArea(col + i, row);
                }
            }
            return true;
        } else return false;
    }

    void addCellToBoard(int col, int row, CellState state) {
        board[row][col] = new Cell(col, row, state);
    }

    /**
     * Checks if given size and orientation ship can fit on the board from given coordinates.
     *
     * @param row      Starting row
     * @param col      Starting column
     * @param size     ship size (2..5)
     * @param vertical false - horizontal, true - vertical
     * @return return true if ship can fit, false otherwise
     */
    private boolean canShipFit(int row, int col, int size, boolean vertical) {
        if (row < 0 || col < 0) return false;
        if (vertical) {
            if (row + size >= rows) return false;
            for (int i = 0; i < size; i++) {
                if (board[row + i][col].getState() != CellState.EMPTY) return false;
            }
            return true;
        } else {
            if (col + size >= cols) return false;
            for (int i = 0; i < size; i++) {
                if (board[row][col + i].getState() != CellState.EMPTY) return false;
            }
            return true;
        }
    }

    List<Cell> target(int col, int row) {
        if (row < 0 || row > rows) {
            System.out.println("TARGET: row out of bounds");
            return null;
        }

        List<Cell> result = new ArrayList<>();
        // board should never have null objects
        switch (board[row][col].getState()) {
            case EMPTY:
            case SHIP_AREA:
                board[row][col].setState(CellState.MISS);
                result.add(board[row][col].copy());
                return result;
            case SHIP:
                board[row][col].setState(CellState.HIT);
                result.add(board[row][col].copy());
                List<Cell> ship = checkShipHP(col, row);
                return ship == null ? result : ship;
            case MISS:
            case HIT:
                break;
        }
        return null;
    }

    private List<Cell> checkShipHP(int col, int row) {

        List<Cell> ship = new ArrayList<>();
        // adding current coordinates to list as we know its a confirmed HIT
        ship.add(new Cell(col, row, CellState.HIT));

        // check to left
        for (int i = col - 1; i >= 0; i--) {
            Cell cell = getCell(i, row);
            if (cell == null) break;

            if (cell.getState() == CellState.SHIP || cell.getState() == CellState.HIT)
                ship.add(cell.copy());
            else break;
        }

        // check to right
        for (int i = col + 1; i < 10; i++) {
            Cell cell = getCell(i, row);
            if (cell == null) break;

            if (cell.getState() == CellState.SHIP || cell.getState() == CellState.HIT)
                ship.add(cell.copy());
            else break;
        }

        // check up
        for (int i = row - 1; i >= 0; i--) {
            Cell cell = getCell(col, i);
            if (cell == null) break;

            if (cell.getState() == CellState.SHIP || cell.getState() == CellState.HIT)
                ship.add(cell.copy());
            else break;
        }

        // check down
        for (int i = row + 1; i < 10; i++) {
            Cell cell = getCell(col, i);
            if (cell == null) break;

            if (cell.getState() == CellState.SHIP || cell.getState() == CellState.HIT)
                ship.add(cell.copy());
            else break;
        }

        // count how many hits ship has
        long hits = ship.stream().filter(cell -> cell.getState() == CellState.HIT).count();

        if (debugger.DEBUG) {
            for (Cell cell : ship)
                System.out.print(" { " + cell.getCol() + "," + cell.getRow() + " } ");
            System.out.println("Ship size: " + ship.size() + " | Hits: " + hits);
        }

        // if size is same as amount of hits, means ship has sunk
        if (ship.size() == hits) return ship;
        // ship still alive
        return null;
    }

    void markSunkShip(List<Cell> ship) {
        for (Cell cell : ship)
            markNeighbours(cell.getCol(), cell.getRow(), CellState.MISS);
    }

    /**
     * Prints board state into terminal
     */
    List<String> drawBoard() {
        List<String> asciiBoard = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        //      R   E   S   P   U   B   L   I   K   A
        //    ┬───┼───┼───┼───┼───┼───┼───┼───┼───┼───┐
        //  1 │ o   o   ○
        //    ┼   +   +   +   +   ┼   ┼   ┼   ┼   ┼   ┤
        //    │
        //    └───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
        sb.append("  ");
        for (TopTargetingBar x : TopTargetingBar.values()) {
            sb.append("   ").append(x.name());
        }
        asciiBoard.add(sb.toString());


        asciiBoard.add("   ┬───┼───┼───┼───┼───┼───┼───┼───┼───┼───┐");

        for (int row = 0; row < rows; row++) {
            asciiBoard.add(String.format(" " + row + " │%39s│", ""));

            if (row < rows - 1)
                asciiBoard.add("   ┼   +   +   +   +   +   +   +   +   +   ┤");
            else
                asciiBoard.add("   └───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘");
        }
        return asciiBoard;
    }

    List<Cell> getCellsByState(CellState state) {
        List<Cell> cells = new ArrayList<>();
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++) {
                if (board[i][j].getState() == state) {
                    cells.add(board[i][j].copy());
                }
            }
        return cells;
    }

    void setDebugger(Debugger debugger) {
        this.debugger = debugger;
    }
}
