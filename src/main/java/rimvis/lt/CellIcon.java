package rimvis.lt;

public enum CellIcon {
    EMPTY("~"), SHIP("■"), SHIP_AREA("∙"), HIT("■"), MISS("○"),
    SPLASH_SCREEN("██████╗  █████╗ ████████╗████████╗██╗     ███████╗███████╗██╗  ██╗██╗██████╗ \n" +
            "██╔══██╗██╔══██╗╚══██╔══╝╚══██╔══╝██║     ██╔════╝██╔════╝██║  ██║██║██╔══██╗\n" +
            "██████╔╝███████║   ██║      ██║   ██║     █████╗  ███████╗███████║██║██████╔╝\n" +
            "██╔══██╗██╔══██║   ██║      ██║   ██║     ██╔══╝  ╚════██║██╔══██║██║██╔═══╝ \n" +
            "██████╔╝██║  ██║   ██║      ██║   ███████╗███████╗███████║██║  ██║██║██║     \n" +
            "╚═════╝ ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚══════╝╚══════╝╚══════╝╚═╝  ╚═╝╚═╝╚═╝     \n");

    private String val;

    CellIcon(String val) {
        this.val = val;
    }

    public String[] toLineArray() {
        return this.val.split("\n");
    }

    @Override
    public String toString() {
        return val;
    }
}
