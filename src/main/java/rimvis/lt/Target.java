package rimvis.lt;

class Target {
    private TopTargetingBar col;
    private Integer row;

    void reset() {
        col = null;
        row = null;
    }

    void deleteLast() {
        if (isRowSet()) setRow(null);
        else if (isColSet()) setCol(null);
    }

    void setCol(int col) {
        this.col = TopTargetingBar.values()[col];
    }

    boolean isColSet() {
        return col != null;
    }

    boolean isRowSet() {
        return row != null;
    }

    void setCol(TopTargetingBar col) {
        this.col = col;
    }

    void setRow(Integer row) {
        this.row = row;
    }

    TopTargetingBar getColEnum() {
        return col;
    }

    Integer getCol() {
        return col.ordinal();
    }

    Integer getRow() {
        return row;
    }
}
