package rimvis.lt;

/**
 * Board Cell
 */
class Cell {
    private int row;
    private int col;
    private CellState state;

    Cell() {
    }

    Cell(CellState state) {
        this.state = state;
    }

    Cell(int col, int row, CellState state) {
        this.state = state;
        setCoords(col, row);
    }

    /**
     * Cell coordinates (column, row) setter
     *
     * @param col column
     * @param row row
     */
    void setCoords(int col, int row) {
        this.col = col;
        this.row = row;
    }

    /**
     * Clones cell
     *
     * @return Returns a copy of this cell
     */
    Cell copy() {
        return new Cell(col, row, state);
    }

    /*
        DEFAULT GETTERS & SETTERS
     */
    CellState getState() {
        return state;
    }

    void setState(CellState state) {
        this.state = state;
    }

    int getRow() {
        return row;
    }

    int getCol() {
        return col;
    }
}