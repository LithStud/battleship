package rimvis.lt;

public class Main {

    public static void main(String[] args) {
        Game game = new Game(new Debugger());
        game.init(
                new Player(new Board(), new Board(), false), // player One - Human
                new Player(new Board(), new Board(), true)   // player Two - PC
        );
    }
}
