package rimvis.lt;

public enum CellState {
    EMPTY, SHIP, SHIP_AREA, MISS, HIT, REPEAT_SHOT
}
