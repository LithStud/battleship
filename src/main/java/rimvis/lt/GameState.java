package rimvis.lt;

public enum GameState {
    SPLASH_SCREEN, SETUP, GAME, GAME_OVER, ROWS(30), COLS(100);
    private int val;

    GameState() {
    }

    GameState(int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }
}
