package rimvis.lt;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.IOException;
import java.util.List;

class Draw {

    private Terminal terminal;
    private TextGraphics txt;
    private Debugger debugger = new Debugger(); // should be set from Main.init()

    Draw(Terminal terminal) {
        this.terminal = terminal;
        try {
            setup();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setup() throws IOException {
        txt = terminal.newTextGraphics();
        terminal.setCursorVisible(false);

        checkTerminalSize();
    }

    private boolean checkTerminalSize() throws IOException {
        // check screen size
        boolean isGood = true;

        txt.setForegroundColor(TextColor.ANSI.WHITE);

        int row = 0;
        if (terminal.getTerminalSize().getRows() < GameState.ROWS.getVal()) {
            txt.putString(0, row++, "Please increase terminal height and press F5.");
            isGood = false;
        }

        if (terminal.getTerminalSize().getColumns() < GameState.COLS.getVal()) {
            txt.putString(0, row, "Please increase terminal width and press F5.");
            isGood = false;
        }

        terminal.flush();

        return isGood;
    }

    void setTextColor(TextColor color) {
        txt.setForegroundColor(color);
    }

    void drawText(int col, int row, String text) throws IOException {
        txt.putString(col, row, text);
        terminal.flush();
    }

    private void drawDebug() throws IOException {
        if (debugger.DEBUG) {
            String debug = "DEBUG";
            txt.setForegroundColor(TextColor.ANSI.RED);
            txt.putString(terminal.getTerminalSize().getColumns() / 2 - debug.length() / 2, 0, debug);
            terminal.flush();
        }
    }

    void clearScreen() throws IOException {
        terminal.clearScreen();
    }

    void drawBoard(Board board, int colOffset, int rowOffset) throws IOException {
        if (!checkTerminalSize()) return;

        // Draw debug indicator
        drawDebug();

        // Draw board
        List<String> asciiBoard = board.drawBoard();
        txt.setForegroundColor(TextColor.Indexed.fromRGB(100, 100, 100));
        for (int i = 0; i < asciiBoard.size(); i++)
            txt.putString(colOffset, i + rowOffset, asciiBoard.get(i));

        // Draw empty cells
        drawLayer(board, CellState.EMPTY, CellIcon.EMPTY.toString(), TextColor.ANSI.BLUE, colOffset, rowOffset);

        // Draw ship area according to what debug is set as
        drawLayer(board, CellState.SHIP_AREA,
                debugger.DEBUG ? CellIcon.SHIP_AREA.toString() : CellIcon.EMPTY.toString(),
                debugger.DEBUG ? TextColor.ANSI.MAGENTA : TextColor.ANSI.BLUE,
                colOffset, rowOffset);

        // Draw ships
        drawLayer(board, CellState.SHIP, CellIcon.SHIP.toString(), TextColor.ANSI.GREEN, colOffset, rowOffset);

        // Draw missed shots
        drawLayer(board, CellState.MISS, CellIcon.MISS.toString(), TextColor.ANSI.YELLOW, colOffset, rowOffset);

        // Draw hits
        drawLayer(board, CellState.HIT, CellIcon.HIT.toString(), TextColor.ANSI.RED, colOffset, rowOffset);

        terminal.setCursorPosition(TerminalPosition.TOP_LEFT_CORNER);
    }

    void drawLayer(Board board, CellState layer, String icon, TextColor txtColor, int colOffset, int rowOffset) throws IOException {
        txt.setForegroundColor(txtColor);
        List<Cell> cells = board.getCellsByState(layer);
        for (Cell cell : cells)
            txt.putString(
                    coord2terminal(cell.getCol(), colOffset, true),
                    coord2terminal(cell.getRow(), rowOffset, false),
                    icon
            );
        terminal.flush();
    }

    void splashScreen(boolean blink) {
        try {
            if (!checkTerminalSize()) return;

            txt.setForegroundColor(TextColor.ANSI.RED);
            String[] splash = CellIcon.SPLASH_SCREEN.toLineArray();
            int maxLength = 0;
            for (String line : splash) maxLength = Math.max(maxLength, line.length());
            for (int i = 0; i < splash.length; i++) {
                txt.putString(terminal.getTerminalSize().getColumns() / 2 - maxLength / 2,
                        terminal.getTerminalSize().getRows() / 2 - splash.length / 2 + i, splash[i]);
                if (blink && i == splash.length - 1) {
                    txt.setForegroundColor(TextColor.ANSI.WHITE);
                    String prompt = "Press ENTER";
                    txt.putString(terminal.getTerminalSize().getColumns() / 2 - prompt.length() / 2,
                            terminal.getTerminalSize().getRows() / 2 - splash.length / 2 + i + 2, prompt);
                }
            }
            terminal.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void drawTarget(Target target) throws IOException {
        //terminal.setCursorPosition(26, 0);
        int cols = terminal.getTerminalSize().getColumns();
        txt.putString(0, 26, String.format("%" + cols + "s", ""));
        terminal.flush();
        String text = "Target: ";
        String coords = !target.isColSet() ? "" : target.getColEnum().name()
                + (!target.isRowSet() ? "" : target.getRow().toString());
        //System.out.println("Coords: " + coords);
        txt.putString(terminal.getTerminalSize().getColumns() / 2 - text.length() / 2 - 1, 26,
                text + coords);
        terminal.flush();
    }

    /**
     * BLACK MAGIC > coordinates to terminal rows and columns conversion
     *
     * @param coord    column or row to convert
     * @param offset   offset if needed
     * @param isColumn true to convert column coords, false for row coords
     * @return converted column/row coordinate to terminal col/row
     */
    private int coord2terminal(int coord, int offset, boolean isColumn) {
        //board col or row * cell size + center in cell + terminal offset + sidebar size (eg. " 1 ")
        return isColumn ?
                coord * 4 + 2 + offset + 3 : // column conversion
                coord * 2 + 1 + offset + 1; // row conversion
    }

    void setDebugger(Debugger debugger) {
        this.debugger = debugger;
    }
}
