package rimvis.lt;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Random;

class Game {

    private static final int dRowOffset = 2; // defense board offset
    private static final int dColOffset = 1; // defense board offset
    private static final int aRowOffset = 2; // attack board offset
    private static final int aColOffset = 1 + 50; // attack board offset

    private Debugger debugger;

    private Player playerOne;
    private Player playerTwo;
    private Player activePlayer;
    private String winner;

    private Draw draw;
    private Terminal terminal;
    private boolean looping;
    private GameState state;

    private Target target = new Target(); // for col row target selection using keyboard;

    Game(Debugger debugger) {
        if (debugger == null)
            this.debugger = new Debugger(); // by default debugging will be off
        else this.debugger = debugger;
    }

    public void init(Player playerOne, Player playerTwo) {

        this.playerOne = playerOne;
        this.playerOne.setDebugger(debugger);

        this.playerTwo = playerTwo;
        this.playerTwo.setDebugger(debugger);

        activePlayer = playerOne; // playerOne always starts first

        // enable game loop
        looping = true;
        state = GameState.SPLASH_SCREEN;

        try {
            // setup Draw class with output to Terminal using Lanterna Lib
            terminal = new DefaultTerminalFactory(System.out, System.in, Charset.forName("UTF8"))
                    .setInitialTerminalSize(new TerminalSize(GameState.COLS.getVal(), GameState.ROWS.getVal()))
                    .createTerminal();
            draw = new Draw(terminal);
            draw.setDebugger(debugger);

            // switch to game terminal (this saves current terminal session)
            terminal.enterPrivateMode();
            // start game loop
            loop();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void quit() throws IOException {
        // restore user terminal session and close terminal
        terminal.exitPrivateMode();
        terminal.close();
    }

    private void loop() throws InterruptedException, IOException {
        // initial pAttackBoard draw
        update();
        boolean blink = false;
        while (looping) {

            switch (state) {
                case SPLASH_SCREEN:
                    draw.clearScreen();
                    draw.splashScreen(blink);
                    blink = !blink;
                    break;
                case SETUP:
                    update();
                    draw.setTextColor(TextColor.ANSI.WHITE);
                    draw.drawText(5, 25, "Press 'Insert' button to replace ships. 'ENTER' to start game.");
                    break;
                case GAME:
                    if (activePlayer.isAI()) {
                        randomTarget();
                        if (debugger.DEBUG) {
                            System.out.println("[AI] chosen target: " + target.getColEnum().name() + target.getRow());
                        }
                        handleShooting();
                    }

                    update();
                    updateTarget();

                    if (isGameOver()) {
                        state = GameState.GAME_OVER;
                        //looping = false;
                        //continue;
                    }

                    break;
                case GAME_OVER:
                    draw.clearScreen();
                    draw.setTextColor(TextColor.ANSI.WHITE);
                    String winTxt = "Game won by: " + winner + "! Press Esc to quit.";
                    draw.drawText(terminal.getTerminalSize().getColumns() / 2 - winTxt.length() / 2,
                            terminal.getTerminalSize().getRows() / 2,
                            winTxt
                    );
                    continue;
            }
            // throttle terminal repaint, but give priority to keyboard events
            // depending on game state it also blocks thread
            keyboardEvent();

            Thread.sleep(250);
        }
        // if loop ended quit game
        quit();
    }

    private boolean isGameOver() {
        List<Cell> pOne = playerOne.getDefenseBoard().getCellsByState(CellState.SHIP);
        List<Cell> pTwo = playerTwo.getDefenseBoard().getCellsByState(CellState.SHIP);
        if (pOne.size() == 0 || pTwo.size() == 0) {
            System.out.println("GAME OVER");
            System.out.println("Winner: Player " + (pOne.size() == 0 ? "Two" : "One"));
            winner = "Player " + (pOne.size() == 0 ? "Two" : "One");
            return true;
        }
        return false;
    }

    private void setup() {
        playerOne.getDefenseBoard().placeShips();
        playerTwo.getDefenseBoard().placeShips();
    }

    private void update() throws IOException {
        draw.clearScreen();
        draw.setTextColor(TextColor.ANSI.GREEN);
        draw.drawText(dColOffset + 2, dRowOffset - 1, "Your Fleet");
        draw.drawBoard(playerOne.getDefenseBoard(), dColOffset, dRowOffset);
        draw.setTextColor(TextColor.ANSI.RED);
        draw.drawText(aColOffset + 2, aRowOffset - 1, "Enemy Fleet");
        draw.drawBoard(playerOne.getAttackBoard(), aColOffset, aRowOffset);
    }

    private void updateTarget() throws IOException {
        draw.drawTarget(target);
    }

    private void randomTarget() {
        List<Cell> possibleTargets = activePlayer.getAttackBoard().getCellsByState(CellState.EMPTY);
        Random random = new Random();
        Cell nextTarget = possibleTargets.get(random.nextInt(possibleTargets.size()));
        target.setCol(nextTarget.getCol());
        target.setRow(nextTarget.getRow());
    }

    private void handleShooting() {
        Player opponent = activePlayer == playerOne ? playerTwo : playerOne;

        switch (activePlayer.shoot(opponent, target)) {
            case REPEAT_SHOT:
                System.out.println("Repeated shot");
                break;
            case HIT:
                System.out.println((activePlayer.isAI() ? "[AI] " : "") + "Good shot. Shoot again.");
                break;
            default:
                System.out.println((activePlayer.isAI() ? "[AI] " : "") + "Shot missed, opponent turn");
                activePlayer = opponent;
                break;
        }
        target.reset();
    }

    private void handleEnter() {
        switch (state) {
            case SPLASH_SCREEN:
                state = GameState.SETUP;
                setup();
                break;
            case SETUP:
                state = GameState.GAME;
                break;
            case GAME:
                if (target.isRowSet() && target.isColSet()) handleShooting();
                break;
        }
    }

    private void editTarget(Character key) {
        if (!target.isColSet()) {
            if (Character.isLetter(key)) {
                try {
                    target.setCol(
                            TopTargetingBar.valueOf(
                                    key.toString().toUpperCase())
                    );
                } catch (IllegalArgumentException e) {
                    // ignore letters that are not part of targeting
                }

            }
        } else if (!target.isRowSet()) {
            if (Character.isDigit(key)) {
                Integer row = Integer.valueOf(key.toString());
                if (row.compareTo(10) < 0)
                    target.setRow(row);
            }
        }
    }

    private void keyboardEvent() throws IOException {
        // on splash screen we want to see animation, anywhere else terminal prompt should block thread;
        // for keyboard handling
        KeyStroke keyStroke;
        if (state == GameState.SPLASH_SCREEN || activePlayer.isAI()) // if AI shouldnt block thread
            keyStroke = terminal.pollInput();
        else keyStroke = terminal.readInput();

        if (keyStroke != null) {
            KeyType key = keyStroke.getKeyType();
            switch (key) {
                case EOF:
                case Escape:
                    looping = false;
                    break;
                case Enter:
                    handleEnter();
                    break;
                case Insert:
                    if (state == GameState.SETUP) {
                        System.out.println("Game reset");
                        playerOne.resetBoards();
                        playerTwo.resetBoards();
                        target.reset();
                    }
                    break;
                case F5:
                    System.out.println("Refresh.");
                    break;
                case F12:
                    debugger.DEBUG = !debugger.DEBUG;
                    System.out.println("Debug " + (debugger.DEBUG ? "ON" : "OFF"));
                    System.out.println("Screen size: " + terminal.getTerminalSize().toString());
                    break;
                case F9:
                    if (debugger.DEBUG) state = GameState.SPLASH_SCREEN;
                    break;
                case Backspace:
                    if (state == GameState.GAME && !activePlayer.isAI()) {
                        target.deleteLast();
                    }
                    break;
                default:
                    // only during human player he should be allowed to type in target coords
                    if (state == GameState.GAME && !activePlayer.isAI() && key == KeyType.Character) {
                        editTarget(keyStroke.getCharacter());
                    }
                    break;
            }
        }
    }
}
