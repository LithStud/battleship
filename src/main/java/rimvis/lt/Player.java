package rimvis.lt;

import java.util.List;

class Player {

    // debugger should be set from Game.init() method
    private Debugger debugger = new Debugger(); // just some default debugger (default: OFF)

    private boolean AI;
    private Board defenseBoard;
    private Board attackBoard;

    Player(Board defenseBoard, Board attackBoard, boolean AI) {
        this.AI = AI;
        this.defenseBoard = defenseBoard;
        this.attackBoard = attackBoard;
    }

    CellState shoot(Player opponent, Target target) {
        // always returns either null, 1 Cell or a list of Cells marking ship location
        List<Cell> result = opponent.getDefenseBoard().target(target.getCol(), target.getRow());
        // we have something
        if (result != null) {
            //System.out.println("It was: " + result.getState().name());
            // if we got just 1 cell means its a miss of hit (without sinking ship)
            if (result.size() == 1) {
                attackBoard.addCellToBoard(result.get(0).getCol(), result.get(0).getRow(), result.get(0).getState());
                return result.get(0).getState();
            } else { // more than one cell means we got back a list of cells representing sunken ship
                // since we got back ship we can use target coords to mark last hit
                attackBoard.addCellToBoard(target.getCol(), target.getRow(), CellState.HIT);
                // lets mark all surrounding cells as misses to notify player he sunk ship
                attackBoard.markSunkShip(result);
                return CellState.HIT;
            }
        }
        // null means this cell was already targeted (or somehow managed to be out of bounds)
        return CellState.REPEAT_SHOT;
    }

    void resetBoards() {
        attackBoard.resetBoard();
        defenseBoard.resetBoard();
        defenseBoard.placeShips();
    }

    void setDebugger(Debugger debugger) {
        this.debugger = debugger;
        // pass it onto Board objects
        attackBoard.setDebugger(debugger);
        defenseBoard.setDebugger(debugger);
    }

    boolean isAI() {
        return AI;
    }

    Board getDefenseBoard() {
        return defenseBoard;
    }

    Board getAttackBoard() {
        return attackBoard;
    }
}
